-------------------------------------------------------------------------------
create or replace 
procedure reiniciar_SQ_USER(SQ_USER in varchar2 )
is
    L_VAL number;
begin
    execute immediate
    'select ' || SQ_USER || '.nextval from dual' INTO L_VAL;
    execute immediate
    'alter sequence ' || SQ_USER || ' increment by -' || L_VAL || ' minvalue 0';
    execute immediate
    'select ' || SQ_USER || '.nextval from dual' INTO L_VAL;
    execute immediate
    'alter sequence ' || SQ_USER || ' increment by 1 minvalue 0';
end;
--------------------------------------------------------------------------
BEGIN 
    reiniciar_SQ_USER('SQ_USER');
END;
--------------------------------------------------------------------------
BEGIN 
    INSERT_USER('EDWIN','EXWIN@HOTMAIL.COM','ERMES123','INACTIVO');
END;
-------------------------------------------------------------------------
select SQ_USER.nextval from dual;

--------------------------------------------------------------------------
CREATE SEQUENCE SQ_USER
INCREMENT BY 1
MAXVALUE 99999999999
START WITH 1
CACHE 20;
------------------------------
CREATE TABLE HR_USER(
USER_ID NUMBER,
USER_NAME VARCHAR2(30),
USER_CORREO VARCHAR2(50),
USER_CLAVE VARCHAR2(50),
USER_ESTADO VARCHAR2(15)
)
-------------------------------
INSERT INTO HR_USER VALUES (SQ_USER.NEXTVAL,'XAVIER','THEXAVI313@HOTMAIL.COM','XAVIER123','ACTIVO');
COMMIT;
ROLLBACK;
-------------------------------
SELECT*FROM HR_USER
SELECT*FROM HR_USER where user_id=2;

SELECT COUNT(USER_ID) FROM HR_USER;
---------------------
CREATE TABLE IMAGENES(
IMG_ID NUMBER,
IMAGEN BLOB
);
---------------------
SELECT *FROM  employees
SELECT EMPLOYEE_ID,FIRST_NAME,LAST_NAME,EMAIL, PHONE_NUMBER FROM EMPLOYEES;
-----------------------------------------
create directory IMAGEN_DIR as 'C:\oraclexe\imagenes';
-----------------------------------------
grant read on directory IMAGEN_DIR to xavier;
-----------------------------------------
CREATE OR REPLACE PROCEDURE agregarImagen
   (directorio IN varchar2, archivoImagen IN varchar2) as
   f_lob bfile;
   b_lob blob;
   ID_IMG NUMBER;
BEGIN
   SELECT COUNT(IMG_ID)+1 INTO ID_IMG
   FROM IMAGENES; 

   f_lob := bfilename(directorio, archivoImagen);
   INSERT INTO IMAGENES VALUES ( ID_IMG, empty_blob() )
   RETURNING IMAGEN into b_lob;
   --Abrir archivo
   dbms_lob.fileopen(f_lob,dbms_lob.file_readonly);
   --Leer archivo
   dbms_lob.loadfromfile(b_lob, f_lob, dbms_lob.getlength(f_lob));
   --Cerrar archivo
   dbms_lob.fileclose(f_lob);
   commit;
END agregarImagen;
-----------------------------------------
BEGIN
agregaRImagen('IMAGEN_DIR','xt.jpg');
END;
---------------------------------------------
SELECT * FROM IMAGENES;



