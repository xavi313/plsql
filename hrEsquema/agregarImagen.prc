create or replace procedure AGREGARIMAGEN(DIRECTORIO IN VARCHAR2, ARCHIVOIMAGEN IN VARCHAR2) is
   f_lob bfile;
   b_lob blob;
BEGIN
   f_lob := bfilename(directorio, archivoImagen);
   INSERT INTO IMAGENES VALUES ( 'id', empty_blob() )
   RETURNING IMAGEN into b_lob;
   --Abrir archivo
   dbms_lob.fileopen(f_lob,dbms_lob.file_readonly);
   --Leer archivo
   dbms_lob.loadfromfile(b_lob, f_lob, dbms_lob.getlength(f_lob));
   --Cerrar archivo
   dbms_lob.fileclose(f_lob);
   commit;
end AGREGARIMAGEN;
/
