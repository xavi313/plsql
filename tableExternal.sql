
CREATE or replace DIRECTORY dir_xavier AS 'C:\dir_rgt_Xavier';
CREATE or replace DIRECTORY rgt_dir_xavier AS 'D:\rgt_reportes_tmp';


grant read,write on directory dir_xavier to hr;


SELECT * FROM ALL_DIRECTORIES for update;

CREATE table rgt_text_proyectos
      ( 
      id_servicio varchar2(15),
      codig_doc varchar2(30),
      status varchar2(15),
      desc_status varchar(8)
      )
      organization external
      (
      TYPE ORACLE_LOADER
      default directory dir_xavier
      access parameters
      (
      fields terminated by ';'
      )
    location ('status.csv')
    )
    REJECT LIMIT UNLIMITED;
    
select R.*,R.ROWID from rgt_text_proyectos R;
ALTER TABLE rgt_text_proyectos DROP COLUMN FECHA_REACTIVACION VARCHAR2(20);

