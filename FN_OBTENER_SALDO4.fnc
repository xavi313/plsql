create or replace function FN_OBTENER_SALDO4(PV_JOB_ID IN VARCHAR2) return number is
  LN_SAL_MIN  number;
begin
  SELECT MIN_SALARY INTO LN_SAL_MIN
  FROM JOBS
  WHERE JOB_ID =PV_JOB_ID;
  return(LN_SAL_MIN);
end FN_OBTENER_SALDO4;
/
