/*practicas con inner join*/
select *from departments; 
select *from employees;
select *from job_history; 
select *from jobs;
select *from locations; 
select *from countries; 
select *from regions;
/*
UPDATE HR.COUNTRIES C SET C.REGION_ID=1 WHERE C.COUNTRY_ID IN ('IT', 'UK','BE','CH','DK','FR','NL');
UPDATE HR.COUNTRIES C SET C.REGION_ID=3 WHERE C.COUNTRY_ID IN ( 'CN','AU','IN','HK','SG');
UPDATE HR.COUNTRIES C SET C.REGION_ID=4 WHERE C.COUNTRY_ID IN ('KW', 'CN','IL','NG','ZM','ZW');
UPDATE HR.REGIONS R SET R.REGION_NAME='NORTH AMERICA' WHERE R.REGION_ID=2;
INSERT INTO HR.REGIONS (REGION_ID, REGION_NAME) VALUES (5, 'LATIN AMERICA')
COMMIT;*/
------------------------------------------------------------------------------------------------------------------
SELECT CONCAT (E.FIRST_NAME|| ' ',E.LAST_NAME) NOMBRE,E.EMAIL,E.SALARY,CONCAT (J.JOB_TITLE||'(',D.DEPARTMENT_NAME||')'),CONCAT (C.COUNTRY_NAME||'(',R.REGION_NAME||')') UBICACION
FROM LOCATIONS L
INNER JOIN HR.COUNTRIES  C ON C.COUNTRY_ID = L.COUNTRY_ID
INNER JOIN HR.REGIONS  R ON R.REGION_ID = C.REGION_ID
INNER JOIN HR.DEPARTMENTS D ON D.LOCATION_ID= L.LOCATION_ID
INNER JOIN HR.EMPLOYEES E ON E.DEPARTMENT_ID= D.DEPARTMENT_ID
INNER JOIN HR.JOBS J ON J.JOB_ID= E.JOB_ID
---------------------------------------------------------------------------------------------------------------
/*ACTUALIZAR EL SALARIO DEL EMPLEADO DEPARTAMENTO Y CARGO A OCUPAR*/
DECLARE
BEGIN
EXCEPTION
END;



-----******************************************************************************************************-----
SELECT *
FROM HR.COUNTRIES CO, HR.DEPARTMENTS DE, HR.LOCATIONS LO, HR.REGIONS RE, HR.EMPLOYEES EM
WHERE DE.DEPARTMENT_ID= EM.DEPARTMENT_ID
AND DE.LOCATION_ID= LO.LOCATION_ID
AND LO.COUNTRY_ID= CO.COUNTRY_ID
AND CO.REGION_ID= RE.REGION_ID

select*from system.tb_clientes;
insert into system.tb_clientes (id_cliente,cli_cedula,cli_nombre,cli_apellido,cli_telf,cli_direccion)
values(&id_cliente,&cli_cedula,&cli_nombre,&cli_apellido,&cli_telf,&cli_direccion);

select distinct e.department_id from hr.employees e;

select e.last_name,e.department_id from hr.employees e
where e.department_id is null;

CREATE VIEW
select TO_CHAR(SALARY, '$99,999.00') SALARY
FROM EMPLOYEES
WHERE LAST_NAME='Ernst';

